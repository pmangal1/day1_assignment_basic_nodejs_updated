const { add, substract, multiply, divide } = require('./operations');
const { calcArt } = require('./calculatorAsciiArt');
let prompt = require('prompt');
let colors = require("colors/safe");

// empty prompt message
prompt.message = "";
prompt.colors = false;

// user input properties with validations
let inputSchema = {
  properties: {
    numberOne: {
      description: colors.rainbow('\nEnter First Number'),
      pattern: /^\d+$/,
      message: 'input should be number',
      required: true
    },
    numberTwo: {
      description: colors.rainbow('Enter Second Number'),
      pattern: /^\d+$/,
      message: 'input should be number',
      required: true
    },
    choice: {
      description: colors.blue('\nSelect your choice:\n1. Add\n2. Subtraction\n3. Multiply\n4. Divide\n5. Press Any other key to Exit\n'),
      message: 'input should be number',
      required: true
    }
  }
};

console.log(calcArt);

// Start the prompt
prompt.start();

const getNumbersFromUser = () => {
  prompt.get(inputSchema, function (err, result) {
    if (err) { return onErr(err); }

    let choice = Number(result.choice);
    let numberOne = Number(result.numberOne);
    let numberTwo = Number(result.numberTwo);
    let ans;

    if (choice === 1)
      ans = add(numberOne, numberTwo);
    else if (choice === 2)
      ans = substract(numberOne, numberTwo);
    else if (choice === 3)
      ans = multiply(numberOne, numberTwo);
    else if (choice === 4)
      ans = divide(numberOne, numberTwo);
    else {
      // exit condition
      console.log('Bye Bye !!');
      process.exit();
    }

    console.log(ans);
    getNumbersFromUser(); // call again if user wants to continue
  });

  function onErr(err) {
    console.log(err);
    return 1;
  }

}

getNumbersFromUser(); // get numbers from users