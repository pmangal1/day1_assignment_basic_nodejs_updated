const rlp = require('readline');
const { add, substract, multiply, divide } = require('./operations')

// create interface from readline
const rl = rlp.createInterface({
  input: process.stdin,
  output: process.stdout
});

// ask input from user
function ask(message) {
  return new Promise((resolve, reject) => {
    rl.question(message, (input) => resolve(input));
  });
}

// validate input string and return number
const validateNumber = (number) => {
  if (isNaN(number)) {
    console.log('Wrong User Input.. Try Again !!');
    getInputFromUser();
  }
  return Number(number)
}

const getInputFromUser = () => {
  let choice, numberOne, numberTwo, ans;
  ask('Select Your Choice:\n1. Addition\n2. Substraction\n3. Multiplication\n4. Division\n5. Exit\n')
    .then((result) => {
      choice = validateNumber(result);
      if (choice >= 5) process.exit()
      return ask('Enter First Number: ');
    })
    .then((result) => {
      numberOne = validateNumber(result);
      return ask('Enter Second Number: ');
    })
    .then(result => {
      numberTwo = validateNumber(result);
      
      // condition on choice
      if (choice === 1)
        ans = add(numberOne, numberTwo)
      else if (choice === 2)
        ans = substract(numberOne, numberTwo)
      else if (choice === 3)
        ans = multiply(numberOne, numberTwo)
      else
        ans = divide(numberOne, numberTwo)

      console.log(ans)
      getInputFromUser() // calling function again for more input
    });
}
getInputFromUser()

