const fs = require("fs");
const jobs = require("./data/jobs.json");
const technologies = require("./data/technologies.json");

const createTags = (jobsJson, technologiesJson) => {
  // map of technologies
  let processedTechnologiesData = new Map();

  // converted technologies into key(lowercase) value(original) pair  to map
  technologiesJson.map(technology => {
    processedTechnologiesData.set(technology.toLowerCase(), technology);
  })

  // processing jobs
  for (let i = 0; i < jobsJson.length; i++) {
    let tags = new Set(); // set for technologies tag
    let processingTimeStarts = new Date().getTime() // time starts
    // splitting description with spaces
    jobsJson[i].description.split(' ').map(word => {

      // removing begining and end dot if any and coverting to lowercase
      let processedWord = word.replace(/^\.+/, "").replace(/\.+$/, "").toLowerCase();
      if (processedTechnologiesData.has(processedWord))
        tags.add(processedTechnologiesData.get(processedWord));
    })

    jobsJson[i].tags = [...tags].join(', ');
    jobsJson[i].processing_timestamp = new Date().getTime() - processingTimeStarts; // processing done
  }

  // Writing jobs and updated tags to a file 
  fs.writeFile(`./data/${new Date().getTime()}_response.json`, JSON.stringify(jobsJson), err => {
    // Checking for errors 
    if (err) throw err;
    console.log("Done writing"); // Success 
  });

}

createTags(jobs, technologies);

