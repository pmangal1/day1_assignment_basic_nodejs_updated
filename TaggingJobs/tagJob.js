const fs = require("fs");
const jobs = require("./data/jobs.json");
const technologies = require("./data/technologies.json");

const createTags = (jobsJson, technologiesJson) => {
  // processing jobs
  for (let i = 0; i < jobsJson.length; i++) {
    let tags = []; // set for technologies tag
    let processingTimeStarts = new Date().getTime(); // time starts

    // iterate over each technology
    technologiesJson.map(technology => {
      let lowercaseDescription = jobsJson[i].description.toLowerCase(); // lowercase description
      let lowercaseTechnology = technology.toLowerCase(); // lowercase technology

      // condition searching technology for eg. node.js .nodejs nodejs. .nodejs.
      if (lowercaseDescription.includes(' ' + lowercaseTechnology + ' ') || lowercaseDescription.includes('.' + lowercaseTechnology + ' ') || lowercaseDescription.includes(' ' + lowercaseTechnology + '.') || lowercaseDescription.includes('.' + lowercaseTechnology + '.'))
        tags.push(technology);
    })
    jobsJson[i].tags = tags.join(', ');
    jobsJson[i].processing_timestamp = new Date().getTime() - processingTimeStarts; // processing done

  }


  // Writing jobs and updated tags to a file 
  fs.writeFile(`./data/${new Date().getTime()}_response.json`, JSON.stringify(jobsJson), err => {
    // Checking for errors 
    if (err) throw err;
    console.log("Tags Added Sucessfully"); // Success 
  });
}

createTags(jobs, technologies);

